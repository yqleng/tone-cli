#!/usr/bin/awk -f
BEGIN{ FS = ","}
/^[0-9]/ { next }
/^memcpy/ { next }
/^Compress/ { next }

/^(.*),(.*),(.*),(.*),(.*),(.*),(.*)$/ {
    gsub(/ /,"_",$1)
    lzbench_results[$1".compress"] += $2
    lzbench_results[$1".decompress"] += $3
    lzbench_results[$1".ratio"] += $6
    next
}

END {
    for ( name in lzbench_results )
        printf("%s: %.2f\n",name,lzbench_results[name])
}

