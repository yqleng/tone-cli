#!/bin/bash

set_env_from_install()
{
    cd ${TONE_BM_RUN_DIR}/
    rm -rf ${TONE_BM_RUN_DIR}/.shrc
}

build()
{
    :
}

install()
{
    . ${TONE_SUITE_DIR}/jdk-rpm-install-test/common.sh
    logger "set_env_from_install || exit 1"
    if [ "${PKG_CI_REPO_SOURCE_BRANCH}" = "a23" ];then
        diskName=$(df -h | grep '/$' | awk -F ' ' '{print $1}')
        partNum=$(echo ${diskName} | grep -o '[0-9]')
        totalDiskName=$(echo ${diskName} | cut -d "${partNum}" -f 1)
        if [ -n "${totalDiskName}" ] && [ -n "${partNum}" ];then
            growpart ${totalDiskName} ${partNum}
            xfs_growfs /
            df -h
        fi
    fi
    local allTestRpms=$(get_specific_strings ${PKG_CI_ABS_RPM_URL} 0 "$(arch)")
    oldIFS=","
    IFS=","
    local testRpms=(${allTestRpms})
    rm -rf ${TONE_BM_RUN_DIR}/rpm2install/* && mkdir -p ${TONE_BM_RUN_DIR}/rpm2install
    cd ${TONE_BM_RUN_DIR}/rpm2install
    for testRpm in ${testRpms[@]}
    do
        logger "wget_wrapper ${testRpm} ${testRpm##*'/'} ||  exit 1"
    done
    IFS=${oldIFS}
}
