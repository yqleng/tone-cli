#!/bin/bash

. $TONE_ROOT/lib/testinfo.sh

run()
{
	add_testinfo_cpu_model
	./testscripts/ltpstress.sh -n -t $t
}

parse()
{
	k=$(pwd)
	d=$(awk '/[0-9]*\.output1$/{print $0}' $k/stdout.log)
	d2=$(awk '/[0-9]*\.output2$/{print $0}' $k/stdout.log)
	d3=$(awk '/[0-9]*\.output3$/{print $0}' $k/stdout.log)
	echo "output1\n"
	awk -f $TONE_BM_SUITE_DIR/parse.awk $d
	echo "output2\n"
	awk -f $TONE_BM_SUITE_DIR/parse.awk $d2
	echo "output3\n"
	awk -f $TONE_BM_SUITE_DIR/parse.awk $d3
}
