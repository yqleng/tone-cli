#!/usr/bin/awk -f

/^This system uses [0-9]+ bytes per array element/ {
  printf("element_size_byte: %.2f\n",$4)
}

/Array size = [0-9]+ \(elements\)/ {
  printf("array_size: %.2f\n",$4)
}

/^Copy|Scale|Add|Triad/ {
  printf("%s %.2f MB/s\n",$1,$2)
}
