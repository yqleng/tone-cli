#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from utils.log import logger
from module_interfaces import TESTMODULE
from utils.tests import TestCommands


class SystemInfoPre(TESTMODULE):
    priority = 0

    def run(self, test_instance):
        testcmd = TestCommands(test_instance.config)
        testcmd.update_log_path()
        testcmd.dump_system_info(ext='pre')
        testcmd.dump_env()
        print("\r                 ")
