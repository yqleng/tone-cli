# perf-test
## Description
Runs sanity tests of perf tool.

## Version
N/A

## Category
functional

## Parameters
N/A

## Results
```
 1: vmlinux symtab matches kallsyms                       : Skip
 2: Detect openat syscall event                           : Ok
 3: Detect openat syscall event on all cpus               : Ok
 4: Read samples using the mmap interface                 : Ok
......
```

## Manual Run
```
perf test
```
