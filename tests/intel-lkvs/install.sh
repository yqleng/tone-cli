GIT_URL="https://github.com/intel/lkvs.git"
BRANCH="main"

fetch()
{
    git_clone $GIT_URL $TONE_BM_CACHE_DIR
}

build()
{
    if [ ! -d "${TONE_BM_CACHE_DIR}/xsave" ]; then
        fetch
    fi

    cd "$TONE_BM_CACHE_DIR/xsave" || exit 1
    make

    cd "$TONE_BM_CACHE_DIR/tdx-compliance" || exit 1
    make
}

install()
{
    if lsmod | grep -q "tdx_compliance"; then
	$(rmmod tdx_compliance)
    fi

    insmod tdx-compliance.ko

    echo "success in insmod tdx-compliance.ko"
}

