#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from utils.log import logger
from utils.tests import TestCommands
from module_interfaces import TESTMODULE


class StaticReport(TESTMODULE):
    priority = 100

    def run(self, test_instance):
        testcmd = TestCommands(test_instance.config)
        testcmd.static()
