#!/usr/bin/awk -f

/^Copy|Scale|Add|Triad/ {
  printf("%s %.2f MB/s\n",$1,$2)
}
