#!/bin/sh
if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
    DEP_PKG_LIST="linux-tools-common linux-tools-$(uname -r) linux-cloud-tools-$(uname -r)"
else
    DEP_PKG_LIST="perf"
fi

fetch()
{
	echo "Nothing to fetch ..."
}

build()
{
	echo "Nothing to build ..."
}

install()
{
	echo "Nothing to install ..."
}
