#!/bin/bash

export PATH="$TONE_BM_RUN_DIR"/bin:$PATH

opt_ip=
[ "$IP" = 'ipv4' ] && opt_ip='-4'
[ "$IP" = 'ipv6' ] && opt_ip='-6'

tst_cmd="netserver $opt_ip"

# SXH: kill netserver if exists.
ns=$(ps -ef | grep "netserver -" | grep -v grep)
if (( $? == 0 )); then
    pid=$(echo "$ns" | awk '{print $2}')
    kill -9 "$pid"
    sleep 3
fi  

systemctl stop firewalld

# start netserver
timestr=$(date +"%F %T")
echo "${timestr} : ${glb_numa} $tst_cmd"
${glb_numa} $tst_cmd
