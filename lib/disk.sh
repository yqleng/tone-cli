#!/bin/bash

# return: nr_disk disk_type
parse_disk()
{
    local disk=$1
    nr_disk=${disk//[a-Z]}
    # Set default nr_disk to 1
    [ -n "$nr_disk" ] || nr_disk=1
    disk_type=${disk##[0-9]}
    disk_type=${disk_type,,}
}

# setup_disk_for disk nr_disk
# $1 canbe disk, null, brd, nfs3, nfs4, pmem, virtio_fs
setup_disk_fs()
{
    local disk="$1"
    local nr_disk="$2"
    local mounted="$3"
    [ -n "$nr_disk" ] || nr_disk=1
    if [ "${disk,,}" == "disk" ]; then
        # Process disk drive
        if [ -n "$TONE_DISK" ]; then
            disk_opt="--device $TONE_DISK"
        else
            disk_opt="--device disk"
        fi
    else
        disk_opt="--device ${disk,,}"
    fi

    if [ -n "$disk_size" ]; then
        size_opt="--size $disk_size"
    fi

    if [ -n "$nr_disk" ]; then
        part_opt="--partitionnumber $nr_disk"
    fi

    if [ -n "$fs" ]; then
        # Process memory disk
        fs_opt="--filesystem $fs"
    fi

    if [ -n "$mountoption" ]; then
        mountopt="$mountopt --mountopt \"-o $mountoption\""
    else
        mountopt=""
    fi
    if [ -n "$mkfsopt" ]; then
        $TONE_ROOT/bin/disksetup.py $disk_opt $size_opt $part_opt $fs_opt\
        --mkfsopt "${mkfsopt}" $mountopt
    else
        $TONE_ROOT/bin/disksetup.py $disk_opt $size_opt $part_opt $fs_opt $mountopt
    fi
    ret=$?
    [[ $ret == 0 ]] || exit
}

umount_fs()
{
    "$TONE_ROOT"/bin/disksetup.py --umount
}

get_mount_points()
{
    "$TONE_ROOT"/bin/diskinfo.py data_mountpoints
}

get_dev_partition()
{
    "$TONE_ROOT"/bin/diskinfo.py data_partition
}

get_test_disks()
{
    "$TONE_ROOT"/bin/diskinfo.py data_disk
}
