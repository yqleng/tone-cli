#!/bin/bash
MODULE_NAME=tdx-compliance

setup()
{
    if [[ "$feature" == "tdx-compliance" ]]; then
        if ! lsmod | grep -q "tdx_compliance"; then
            modprobe $MODULE_NAME ||
            insmod $TONE_BM_CACHE_DIR/tdx-compliance/tdx-compliance.ko
        fi
    fi
}

check_vm()
{
    out=`LANG=C lscpu |grep -P 'Hypervisor vendor:.*KVM'`
    if [[ "$out" != "" ]]; then
        echo yes
    else
        echo no
    fi
}

# This function is pending on kernel interface
check_tdvm_version()
{
    echo todo
}

run()
{
    # Check each lines in conf/intel-lkvs
    if [[ "$feature" == "tdx-compliance" ]]; then
        if [[ `check_vm` == "yes" ]]; then
            echo all 1.0 > /sys/kernel/debug/tdx/tdx-tests
            cat /sys/kernel/debug/tdx/tdx-tests
        else
            echo "tdx-compliance SKIP"
        fi
    elif [[ "$feature" == "xstate" ]]; then
        $TONE_BM_CACHE_DIR/xsave/xstate_64
    fi
}

teardown()
{
    #find tdx-compliance module, rmmod it
    if [[ "$feature" == "tdx-compliance" ]]; then
        if lsmod | grep -q "tdx_compliance"; then
            modprobe -r $MODULE_NAME
        fi
    fi
    echo done
}

parse()
{
    awk '
	match($0, /^[0-9]+:\s*([^:]+):\s*(PASS|FAIL)/, arr) {
	    print arr[1] ":" arr[2]
        }
	match($0, /^[0-9]+: ([^[]+): \[(PASS|FAIL)\]/, arr) {
	    print arr[1] ":" arr[2]
	}
	'
}
